const Zinko = require("zinko");

class Visa extends Zinko {

  async GET_me(req) {
    return await this.access(req) || [];
  }

  async check(...args) {
    const [req] = args;
    const OPEN_ACCESS = "OPEN ACCESS";
    const userMap = await this.access(req) || [];
    userMap.push(OPEN_ACCESS);
    const moduleMap = req.module.access || {};
    const operationAccess = moduleMap[req.operation] || (async () => "OPEN ACCESS");
    if (!userMap.includes(await operationAccess(...args))) this.onForbidden();
  }

}

module.exports = Visa;
