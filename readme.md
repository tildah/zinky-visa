# Zinky Visa (Featured module)
This module let's you manage user permissions in the project.
## Overview
Permissions are strings, they are stored in an object which keys represent different users roles, each role has an array of permissions.

Then every other module should have an access getter which associates every operation in the module with a permission.

Zinky Visa will check on every request if the user role has the permission to access the operation.

